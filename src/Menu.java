import java.awt.EventQueue;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;

public class Menu {

	private JFrame frame;
	/**
	 * Inicia o aplicativo.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * ria o aplicativo
	 */
	public Menu() {
		initialize();
	}
	/**
	 * Inicialize o conteudo do quadro.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 336, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		JButton btnJogar = new JButton("JOGAR");
		btnJogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new NovoJogo();
				frame.setVisible(false);
			}
		});
		btnJogar.setBounds(45, 70, 145, 47);
		frame.getContentPane().add(btnJogar);
		
		JButton btnSobreOJogo = new JButton("SOBRE O JOGO");
		btnSobreOJogo.setBounds(45, 130, 145, 47);
		frame.getContentPane().add(btnSobreOJogo);
		
		Label lblBemVindoAo = new JLabel("BEM VINDO AO SNAKE");
		lblBemVindoAo.setForeground(Color.WHITE);
		lblBemVindoAo.setFont(new Font("Dialog", Font.BOLD, 15));
		lblBemVindoAo.setBounds(48, 24, 323, 41);
		frame.getContentPane().add(lblBemVindoAo);

		
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("cobra2.jpg")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 336, 500);
		frame.getContentPane().add(lblNewLabel);
	}
}
