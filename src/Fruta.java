import java.util.ArrayList;
import java.util.Random;
import java.awt.Color;
import java.awt.Graphics;

public class Fruit {
	
	private int cordenadaX;
	private int cordenadaY;
	private int largura;
	private int altura;
	private String tipo;
	private int score;
	
	public Fruit(int cordenadaX, int cordenadaY, int tileSize) {
		this.cordenadaX = cordenadaX;
		this.cordenadaY = cordenadaY;
		this.largura = tileSize;
		this.altura = tileSize;
		
		ArrayList<String> tipoFruta = new ArrayList<String>();
		tipoFruta.add("Simple");
		tipoFruta.add("Bomb");
		tipoFruta.add("Big");
		tipoFruta.add("Decrease");
		
		Random random = new Random();
		int r = random.nextInt(4);
		
		this.tipo = tipoFruta.get(r);
		
		//Pontuacao para cada tipo de fruta
		if(tipo == "Simple") {
			this.score = 1;
		}
		if(tipo == "Bomb") {
			this.score = 0;
		}
		if(tipo == "Big") {
			this.score = 2;
		}
		if(tipo == "Decrease") {
			this.score = 0;
		}
	}
	
	public Fruit(int cordenadaX, int cordenadaY, int tileSize,String tipo) {

		this.cordenadaX = cordenadaX;
		this.cordenadaY = cordenadaY;
		this.altura = tileSize;
		this.largura = tileSize;
		this.tipo = tipo;		
		
		if(tipo == "Simple") {
			this.score = 1;
		}
		else if(tipo == "Big") {
			this.score = 2;
		}
		else if(tipo == "Decrease") {
			this.score = 0;
		}
		else if(tipo == "Bomb") {
			this.score = 0;
		}
		else if(tipo == "Obstaculo") {
			this.score = 0;
		}
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void tick() {
	}
	//Funcao que desenha a fruta na tela
	public void draw(Graphics g) {
		//Simple Fruit
		if(tipo == "Simple") {
			g.setColor(Color.RED);
		}
		//Big Fruit
		if(tipo == "Big") {
			g.setColor(new Color(153, 51, 153));
		}
		//Bomb Fruit
		if(tipo == "Bomb") {
			g.setColor(Color.GRAY);
		}
		//Decrease
		if(tipo == "Decrease") {
			g.setColor(Color.YELLOW);
		}
		g.fillRect(cordenadaX*largura, cordenadaY*altura, largura, altura);
	}

	public int getcordenadaX() {
		return cordenadaX;
	}
	public void setcordenadaX(int cordenadaX) {
		this.cordenadaX = cordenadaX;
	}
	public int getcordenadaY() {
		return cordenadaY;
	}
	public void setcordenadaY(int cordenadaY) {
		this.cordenadaY = cordenadaY;
	}

}

