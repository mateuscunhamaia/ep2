import java.awt.Graphics;
import java.awt.Color;

public class Snake {
	
	private int cordenadaX;
	private int cordenadaY;
	private int largura;
	private int altura;
	
	private String tipo = "Comum";
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Snake(int cordenadaX, int cordenadaY, int tileSize, String tipo) {
		this.cordenadaX = cordenadaX;
		this.cordenadaY = cordenadaY;
		this.largura = tileSize;
		this.altura = tileSize;
		this.tipo = tipo;
	}
	public void tick() {
		
	}
	//Funcao que desenha a cobra na tela
	public void draw(Graphics g) {
		//Cobra Comum
		if(this.tipo == "Comum") {
			g.setColor(Color.GREEN);
		}
		//Cobra Kitty
		else if(this.tipo == "Kitty") {
			g.setColor(new Color(255, 0, 127));
		}
		//Cobra Star
		else if(this.tipo == "Star") {
			g.setColor(Color.BLUE);
		}
		g.fillRect(cordenadaX*largura, cordenadaY*altura, largura, altura);
	}
	public int getCordenadaX() {
		return cordenadaX;
	}

	public void setCordenadaX(int cordenadaX) {
		this.cordenadaX = cordenadaX;
	}

	public int getCordenadaY() {
		return cordenadaY;
	}

	public void setCordenadaY(int cordenadaY) {
		this.cordenadaY = cordenadaY;
	}

}
