import java.awt.Graphics;
import java.awt.Color;

public class Obstaculo {
	
	private int cordenadaX;
	private int cordenadaY;
	private int largura;
	private int altura;
	private String tipo;
	private int score;
	
	public Obstaculo(int cordenadaX, int cordenadaY, int tileSize, String tipo) {
		
		this.cordenadaX = cordenadaX;
		this.cordenadaY = cordenadaY;
		this.altura = tileSize;
		this.largura = tileSize;
		this.tipo = tipo;	
		
		if(tipo == "Obstaculo") {
			this.score = 0;
		}
		
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void tick() {
		
	}
	//Obstaculo
	public void draw(Graphics g) {
		if(tipo == "Obstaculo") {
			g.setColor(new Color(244, 76 ,0));
			g.fillRect(cordenadaX*largura, cordenadaY*altura, largura, altura);
		}
	}

	public int getCordX() {
		return cordX;
	}

	public void setCordX(int cordX) {
		this.cordX = cordX;
	}

	public int getCordY() {
		return cordY;
	}

	public void setCordY(int cordY) {
		this.cordY = cordY;
	}

